## Disclaimer

Welcome to the Mafia City Roleplay development area. By accessing this area, you agree to the following:
 - You will never give the script or associated files to anybody who is not on the Mafia City Roleplay Non-Disclosure Agreement.
 - You will delete all development materials (script, databases, associated files, etc.) when you leave the Mafia City Roleplay development team.
 - You will notify the Lead Developer if you notice any security flaws, backdoors, or possible bugs.
